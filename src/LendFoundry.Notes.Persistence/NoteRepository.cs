﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Notes.Persistence
{
    public class NoteRepository : MongoRepository<INote, Note>, INoteRepository
    {
        static NoteRepository()
        {
            BsonClassMap.RegisterClassMap<Note>(map =>
            {
                map.AutoMap();

                var type = typeof(Note);
                map.MapMember(m => m.CreatedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.LastModifiedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public NoteRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "notes")
        {
            CreateIndexIfNotExists("entity-type", Builders<INote>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType));
            CreateIndexIfNotExists("entity-type-id", Builders<INote>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.EntityType).Ascending(i => i.EntityId));
        }

        public async Task<IEnumerable<INote>> GetAllNotes(string entityType, string entityId)
        {
            return await Task.FromResult
            (
                Query.Where(x => x.EntityType == entityType && x.EntityId == entityId).ToList()
            );
        }

        public async Task<IEnumerable<INote>> GetNotesByUser(string entityType, string entityId, string username)
        {
            return await Task.FromResult
            (
                Query.Where(x => x.EntityType == entityType && x.EntityId == entityId && x.CreatedBy == username).ToList()
            );
        }

        public async Task<INote> GetNoteByID(string entityType, string entityId, string id)
        {
            return await Task.FromResult
           (
                Query.FirstOrDefault(x => x.EntityType == entityType && x.EntityId == entityId && x.Id == id)
           );
        }
    }
}