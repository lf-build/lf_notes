﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Threading.Tasks;

namespace LendFoundry.Notes.Api.Controllers
{
    /// <summary>
    /// ApiController
    /// </summary>
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public ApiController(INoteService service, ILogger logger) : base(logger)
        {
            if (service == null) throw new ArgumentException($"{nameof(service)} is mandatory");

            Service = service;
        }

        private INoteService Service { get; }
        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        /// <summary>
        /// AllNotes
        /// </summary>
        /// <param name="entityType">entityType</param>
        /// <param name="entityId">entityId</param>
        /// <returns>INote[]</returns>
        [HttpGet("/{entityType}/{entityId}")]
#if DOTNET2
        [ProducesResponseType(typeof(INote[]), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
#endif
        public async Task<IActionResult> AllNotes(string entityType, string entityId)
        {
            try
            {
                return Ok(await Service.GetAllNotes(entityType, entityId));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// NotesByUser
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <returns>INote</returns>
        [HttpGet("/{entityType}/{entityId}/byuser")]
#if DOTNET2
        [ProducesResponseType(typeof(INote), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
#endif
        public async Task<IActionResult> NotesByUser(string entityType, string entityId)
        {
            try
            {
                return Ok(await Service.GetNotesByUser(entityType, entityId));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// GetNoteById
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="noteId"></param>
        /// <returns>INote</returns>
        [HttpGet("/{entityType}/{entityId}/{noteId}")]
#if DOTNET2
        [ProducesResponseType(typeof(INote), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
#endif
        public async Task<IActionResult> Note(string entityType, string entityId, string noteId)
        {
            try
            {
                return Ok(await Service.GetNote(entityType, entityId, noteId));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// AddNote
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="noteRequest"></param>
        /// <returns>INote</returns>
        [HttpPost("/{entityType}/{entityId}")]
#if DOTNET2
        [ProducesResponseType(typeof(INote), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
#endif
        public async Task<IActionResult> AddNote(string entityType, string entityId, [FromBody] NoteRequest noteRequest)
        {
            try
            {
                return Ok(await Service.AddNote(entityType, entityId, noteRequest));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// UpdateNote
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="noteId"></param>
        /// <param name="noteRequest"></param>
        /// <returns>INote</returns>
        [HttpPut("/{entityType}/{entityId}/{noteId}")]
#if DOTNET2
        [ProducesResponseType(typeof(INote), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
#endif
        public async Task<IActionResult> UpdateNote(string entityType, string entityId, string noteId, [FromBody] NoteRequest noteRequest)
        {
            try
            {
                return Ok(await Service.UpdateNote(entityType, entityId, noteId, noteRequest));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }

        /// <summary>
        /// DeleteNote
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityId"></param>
        /// <param name="noteId"></param>
        /// <returns>bool</returns>
        [HttpDelete("/{entityType}/{entityId}/{noteId}")]
#if DOTNET2
        [ProducesResponseType(typeof(bool), 200)]
        [ProducesResponseType(typeof(BadRequestResult), 400)]
        [ProducesResponseType(typeof(NotFoundResult), 404)]
#endif
        public async Task<IActionResult> DeleteNote(string entityType, string entityId, string noteId)
        {
            try
            {
                await Service.DeleteNote(entityType, entityId, noteId);
                return NoContentResult;
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
            catch (Exception exception)
            {
                return ErrorResult.NotFound(exception.Message);
            }
        }
    }
}