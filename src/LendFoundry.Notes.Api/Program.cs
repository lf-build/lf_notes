﻿using System;
using Microsoft.AspNetCore.Hosting;

namespace LendFoundry.Notes.Api
{
    /// <summary>
    /// Program class
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main method
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {

            var host = new WebHostBuilder()
                .UseKestrel()
                .UseStartup<Startup>()
                .Start("http://*:5000");

            using (host)
            {
                Console.WriteLine("Use Ctrl-C to shutdown the host...");
                host.WaitForShutdown();
            }
        }
    }
}