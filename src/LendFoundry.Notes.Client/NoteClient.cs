﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace LendFoundry.Notes.Client
{
    public class NotesClient : INoteService
    {
        public NotesClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<INote> AddNote(string entityType, string entityId, NoteRequest payload)
        {
            return await Client.PostAsync<NoteRequest, Note>($"/{entityType}/{entityId}", payload, true);
        }

        public async Task DeleteNote(string entityType, string entityId, string noteId)
        {
            await Client.DeleteAsync($"/{entityType}/{entityId}/{noteId}");
        }

        public async Task<IEnumerable<INote>> GetNotesByUser(string entityType, string entityId)
        {
            return await Client.GetAsync<List<Note>>($"/{entityType}/{entityId}/byuser");
        }

        public async Task<IEnumerable<INote>> GetAllNotes(string entityType, string entityId)
        {
            return await Client.GetAsync<List<Note>>($"/{entityType}/{entityId}");
        }

        public async Task<INote> UpdateNote(string entityType, string entityId, string noteId, NoteRequest payload)
        {
            return await Client.PutAsync<NoteRequest, Note>($"/{entityType}/{entityId}/{noteId}", payload, true);
        }

        public async Task<INote> GetNote(string entityType, string entityId, string noteId)
        {
            return await Client.GetAsync<Note>($"/{entityType}/{entityId}/{noteId}");
        }
    }
}