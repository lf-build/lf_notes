﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace LendFoundry.Notes.Client
{
    public class NoteClientFactory : INoteClientFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public NoteClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public NoteClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; set; }
        private Uri Uri { get; }

        public INoteService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("notes");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new NotesClient(client);
        }
    }
}