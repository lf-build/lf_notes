﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Notes.Client
{
    public interface INoteClientFactory
    {
        INoteService Create(ITokenReader reader);
    }
}