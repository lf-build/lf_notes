﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Notes.Client
{
    public static class NoteClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddNoteService(this IServiceCollection services, string endpoint, int port = 5000)
        {
            services.AddTransient<INoteClientFactory>(p => new NoteClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<INoteClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddNoteService(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<INoteClientFactory>(p => new NoteClientFactory(p, uri));
            services.AddTransient(p => p.GetService<INoteClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddNoteService(this IServiceCollection services)
        {
            services.AddTransient<INoteClientFactory>(p => new NoteClientFactory(p));
            services.AddTransient(p => p.GetService<INoteClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}