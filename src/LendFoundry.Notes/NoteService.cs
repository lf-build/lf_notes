﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Notes
{
    public class NoteService : INoteService
    {
        public NoteService
        (
            INoteRepository repository,
            ILogger logger,
            ITenantTime tenantTime,
            ILookupService lookup,
            ITokenReader tokenReader,
            ITokenHandler tokenParser
        )
        {
            if (repository == null) throw new ArgumentException($"{nameof(repository)} is mandatory");
            if (logger == null) throw new ArgumentException($"{nameof(logger)} is mandatory");
            if (tenantTime == null) throw new ArgumentException($"{nameof(tenantTime)} is mandatory");
            if (lookup == null) throw new ArgumentException($"{nameof(lookup)} is mandatory");

            Repository = repository;
            Logger = logger;
            TenantTime = tenantTime;
            Lookup = lookup;
            TokenReader = tokenReader;
            TokenParser = tokenParser;
        }

        private INoteRepository Repository { get; }
        private ILogger Logger { get; }
        private ITenantTime TenantTime { get; }
        private ILookupService Lookup { get; }
        private ITokenReader TokenReader { get; }
        private ITokenHandler TokenParser { get; }

        public async Task<IEnumerable<INote>> GetAllNotes(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            entityType = EnsureEntityTypeIsValid(entityType);

            return await Repository.GetAllNotes(entityType, entityId);
        }

        public async Task<IEnumerable<INote>> GetNotesByUser(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            entityType = EnsureEntityTypeIsValid(entityType);

            return await Repository.GetNotesByUser(entityType, entityId, ExtractCurrentUser());
        }

        public async Task<INote> GetNote(string entityType, string entityId, string noteId)
        {
            if (string.IsNullOrWhiteSpace(noteId))
                throw new ArgumentException($"{nameof(noteId)} is mandatory");

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");

            entityType = EnsureEntityTypeIsValid(entityType);

            var note = await Repository.GetNoteByID(entityType, entityId, noteId);

            if (note != null)
                return note;
            else
                throw new NotFoundException($"Note {noteId} cannot be found");
        }

        public async Task<INote> AddNote(string entityType, string entityId, NoteRequest noteRequest)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            if (string.IsNullOrWhiteSpace(noteRequest.Subject))
                throw new ArgumentException($"{nameof(noteRequest.Subject)} is mandatory");

            entityType = EnsureEntityTypeIsValid(entityType);

            var note = new Note()
            {
                Subject = noteRequest.Subject,
                Description = noteRequest.Description,
                EntityId = entityId,
                EntityType = entityType,
                IsDeleted = false,
                CreatedBy = ExtractCurrentUser(),
                CreatedDate = TenantTime.Now
            };

            await Task.Run(() => Repository.Add(note));

            return note;
        }

        public async Task<INote> UpdateNote(string entityType, string entityId, string noteId, NoteRequest noteRequest)
        {
            return await Update(entityType, entityId, noteId, noteRequest, false);
        }

        public async Task DeleteNote(string entityType, string entityId, string noteId)
        {
            await Update(entityType, entityId, noteId, null, true);
        }

        public async Task<INote> Update(string entityType, string entityId, string noteId, NoteRequest noteRequest, bool IsDeleted)
        {
            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentException($"{nameof(entityId)} is mandatory");
            if (!IsDeleted && string.IsNullOrWhiteSpace(noteRequest.Subject))
                throw new ArgumentException($"{nameof(noteRequest.Subject)} is mandatory");

            entityType = EnsureEntityTypeIsValid(entityType);

            var note = await Repository.GetNoteByID(entityType, entityId, noteId);

            if (note.IsDeleted)
                throw new NotSupportedException($"Note {noteId} is deleted, so you can't do any changes on this note.");

            if (note != null)
            {
                if (IsDeleted)
                {
                    note.IsDeleted = true;
                }
                else
                {
                    note.Subject = noteRequest.Subject;
                    note.Description = noteRequest.Description;
                }

                note.LastModifiedBy = ExtractCurrentUser();
                note.LastModifiedDate = TenantTime.Now;

                Repository.Update(note);

                return note;
            }
            else
            {
                throw new NotFoundException($"Note {noteId} cannot be found");
            }
        }

        private string EnsureEntityTypeIsValid(string entityType)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentException($"{nameof(entityType)} is mandatory");

            entityType = entityType.ToLower();

            if (Lookup.GetLookupEntry("entityTypes", entityType) == null)
                throw new ArgumentException($"{entityType} is not a valid entity");
            return entityType;
        }

        private string ExtractCurrentUser()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            if (string.IsNullOrWhiteSpace(token?.Subject))
                throw new ArgumentException("username is mandatory");
            return token.Subject;
        }
    }
}