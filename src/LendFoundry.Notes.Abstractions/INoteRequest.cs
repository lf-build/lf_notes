﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Notes
{
    public interface INoteRequest
    {
        string Subject { get; set; }
        string Description { get; set; }
    }
}
