﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;

namespace LendFoundry.Notes
{
    public class Note : Aggregate, INote
    {
        public string Subject { get; set; }
        public string Description { get; set; }
        public string EntityType { get; set; }
        public string EntityId { get; set; }
        public bool IsDeleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTimeOffset CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTimeOffset LastModifiedDate { get; set; }
    }
}