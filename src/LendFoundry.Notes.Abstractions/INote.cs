﻿using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Notes
{
    public interface INote : IAggregate
    {
        string Subject { get; set; }
        string Description { get; set; }
        string EntityType { get; set; }
        string EntityId { get; set; }
        bool IsDeleted { get; set; }
        string CreatedBy { get; set; }
        DateTimeOffset CreatedDate { get; set; }
        string LastModifiedBy { get; set; }
        DateTimeOffset LastModifiedDate { get; set; }
    }
}
