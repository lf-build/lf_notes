﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Notes
{
    public interface INoteRepository : IRepository<INote>
    {
        Task<IEnumerable<INote>> GetAllNotes(string entityType, string entityId);
        Task<IEnumerable<INote>> GetNotesByUser(string entityType, string entityId, string username);
        Task<INote> GetNoteByID(string entityType, string entityId, string id);
    }
}