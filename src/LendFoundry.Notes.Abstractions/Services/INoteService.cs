﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Notes
{
    public interface INoteService
    {
        Task<IEnumerable<INote>> GetAllNotes(string entityType, string entityId);

        Task<IEnumerable<INote>> GetNotesByUser(string entityType, string entityId);

        Task<INote> GetNote(string entityType, string entityId, string noteId);

        Task<INote> AddNote(string entityType, string entityId, NoteRequest noteRequest);

        Task<INote> UpdateNote(string entityType, string entityId,string noteId, NoteRequest noteRequest);

        Task DeleteNote(string entityType, string entityId, string noteId);
    }
}