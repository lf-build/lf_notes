﻿namespace LendFoundry.Notes
{
    public class NoteRequest : INoteRequest
    {
        public string Subject { get; set; }
        public string Description { get; set; }
    }
}
