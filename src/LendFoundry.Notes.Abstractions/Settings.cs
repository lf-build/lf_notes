﻿using System;

namespace LendFoundry.Notes
{
    public class Settings
    {        
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "notes";
    }
}