﻿using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using System.Collections.Generic;
using Xunit;

namespace LendFoundry.Notes.Client.Tests
{
    public class NoteServiceClientTests
    {
        private INoteService Client { get; }
        private IRestRequest Request { get; set; }
        private Mock<IServiceClient> ServiceClient { get; }

        public NoteServiceClientTests()
        {
            ServiceClient = new Mock<IServiceClient>();
            Client = new NotesClient(ServiceClient.Object);
        }

        [Fact]
        public void Client_GetAllNotes()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<IEnumerable<INote>>(It.IsAny<IRestRequest>()))
                         .Callback<IRestRequest>(r => Request = r)
                         .ReturnsAsync(It.IsAny<IEnumerable<INote>>());

            Client.GetAllNotes("application", "000001");

            ServiceClient.Verify(x => x.ExecuteAsync<IEnumerable<INote>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
        }

        [Fact]
        public void Client_NotesByUser()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<IEnumerable<INote>>(It.IsAny<IRestRequest>()))
                         .Callback<IRestRequest>(r => Request = r)
                         .ReturnsAsync(It.IsAny<IEnumerable<INote>>());

            Client.GetNotesByUser("application", "000001","test@gmail.com");

            ServiceClient.Verify(x => x.ExecuteAsync<IEnumerable<INote>>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/{username}/user", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
        }

        [Fact]
        public void Client_GetNote()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<INote>(It.IsAny<IRestRequest>()))
                         .Callback<IRestRequest>(r => Request = r)
                         .ReturnsAsync(It.IsAny<INote>());

            Client.GetNote("application", "000001", "1");

            ServiceClient.Verify(x => x.ExecuteAsync<INote>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/{noteId}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
        }

        [Fact]
        public void Client_AddNote()
        {
            ServiceClient.Setup(s => s.Execute<INote>(It.IsAny<IRestRequest>()))
                         .Callback<IRestRequest>(r => Request = r)
                         .Returns(It.IsAny<INote>());

            Client.AddNote("application", "000001", new NoteRequest() { Subject = "Subject 1", Description = "Desc 1" });

            ServiceClient.Verify(x => x.Execute<INote>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
        }

        [Fact]
        public void Client_UpdateNote()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<INote>(It.IsAny<IRestRequest>()))
                         .Callback<IRestRequest>(r => Request = r)
                         .ReturnsAsync(It.IsAny<INote>());

            Client.UpdateNote("application", "000001", "1", new NoteRequest() { Subject = "Subject 1", Description = "Desc 1" });

            ServiceClient.Verify(x => x.ExecuteAsync<INote>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/{noteId}", Request.Resource);
            Assert.Equal(Method.PUT, Request.Method);
        }

        [Fact]
        public void Client_DeleteNote()
        {
            ServiceClient.Setup(s => s.ExecuteAsync<bool>(It.IsAny<IRestRequest>()))
                                      .Callback<IRestRequest>(r => Request = r)
                                      .ReturnsAsync(It.IsAny<bool>());

            Client.DeleteNote("application", "000001", "1", new NoteRequest { UserName = "test@gmail.com" });

            ServiceClient.Verify(x => x.ExecuteAsync<bool>(It.IsAny<IRestRequest>()), Times.Once);

            Assert.Equal("/{entityType}/{entityId}/{noteId}", Request.Resource);
            Assert.Equal(Method.DELETE, Request.Method);
        }
    }
}