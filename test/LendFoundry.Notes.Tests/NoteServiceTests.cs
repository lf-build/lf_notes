﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Notes.Tests
{
    public class NoteServiceTests
    {
        private Mock<INoteRepository> mockNoteRepository { get; set; }
        private Mock<ILogger> mockLogger { get; }
        private Mock<ILookupService> mockLookupService { get; }
        private Mock<ITenantTime> mockTenantTime { get; }
        private NoteService noteService { get; set; }
        private NoteRequest requestNote { get; set; }
        private INote responseNote { get; set; }
        IEnumerable<INote> noteListResponse { get; set; }

        public NoteServiceTests()
        {
            mockNoteRepository = new Mock<INoteRepository>();
            mockLogger = new Mock<ILogger>();
            mockLookupService = new Mock<ILookupService>();
            mockTenantTime = new Mock<ITenantTime>();

        }

        private void MockSetup()
        {
            requestNote = new NoteRequest() { Subject = "Subject 1", Description = "Desc 1", UserName = "test@gmail.com" };
            responseNote = new Note() { Id = "1", Subject = "Subject 1", Description = "Desc 1" };
            noteListResponse = new List<INote>()
            {
                new Note() { Id= "1", Subject="Subject 1", Description="Desc 1"},
                new Note() { Id= "2", Subject="Subject 2", Description="Desc 2"},
            };

            mockLookupService.Setup(x => x.GetLookupEntry("entityTypes", "application")).Returns(new Dictionary<string, string>());
            mockNoteRepository.Setup(x => x.GetNoteByID(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(responseNote));
            mockNoteRepository.Setup(x => x.Add(It.IsAny<Note>())).Verifiable();
            mockNoteRepository.Setup(x => x.Update(It.IsAny<Note>())).Verifiable();
            mockNoteRepository.Setup(x => x.GetAllNotes(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(noteListResponse));
            noteService = new NoteService(mockNoteRepository.Object, mockLogger.Object, mockTenantTime.Object, mockLookupService.Object);
        }

        [Fact]
        public async void GetAllNotes_Success()
        {
            MockSetup();
            var result = await noteService.GetAllNotes("application", "12345");
            Assert.True(result.Count() == 2);
        }

        [Fact]
        public async void GetAllNotes_With_Null_EntityType_EntityId_Should_Thorw_ArgumentException()
        {
            MockSetup();

            await Assert.ThrowsAsync<ArgumentException>(async () =>
             {
                 await noteService.GetAllNotes(null, "12345");
             });

            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await noteService.GetAllNotes("application", null);
            });
        }

        [Fact]
        public async void GetNote_Success()
        {
            MockSetup();
            var result = await noteService.GetNote("application", "12345", "1");
            Assert.True(result.Id == "1");
        }

        [Fact]
        public async void GetNote_With_Null_EntityType_EntityId_Should_Thorw_ArgumentException()
        {
            MockSetup();

            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await noteService.GetNote(null, "12345", "1");
            });

            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await noteService.GetNote("application", null, "1");
            });
        }

        [Fact]
        public void AddNote_Success()
        {
            MockSetup();
            var result = noteService.AddNote("application", "12345", requestNote);
            Assert.True(result.Subject == "Subject 1");
        }

        [Fact]
        public void AddNote_With_Null_EntityType_EntityId_Should_Thorw_ArgumentException()
        {
            MockSetup();

            Assert.Throws<ArgumentException>(() =>
            {
                noteService.AddNote(null, "12345", requestNote);
            });

            Assert.Throws<ArgumentException>(() =>
            {
                noteService.AddNote("application", null, requestNote);
            });
        }

        [Fact]
        public async void UpdateNote_Success()
        {
            MockSetup();
            var result = await noteService.UpdateNote("application", "12345", "1", requestNote);
            Assert.True(result.Id == "1");
        }

        [Fact]
        public async void UpdateNote_With_Null_EntityType_EntityId_Should_Thorw_ArgumentException()
        {
            MockSetup();

            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await noteService.UpdateNote(null, "12345", "1", requestNote);
            });

            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await noteService.UpdateNote("application", null, "1", requestNote);
            });
        }

        [Fact]
        public async void DeleteNote_Success()
        {
            MockSetup();
            var result = await noteService.DeleteNote("application", "12345", "1", new NoteRequest { UserName = "test@gmail.com" });
            Assert.True(result);
        }

        [Fact]
        public async void DeleteNote_With_Null_EntityType_EntityId_Should_Thorw_ArgumentException()
        {
            MockSetup();

            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await noteService.DeleteNote(null, "12345", "1", new NoteRequest { UserName = "test@gmail.com" });
            });

            await Assert.ThrowsAsync<ArgumentException>(async () =>
            {
                await noteService.DeleteNote("application", null, "1", new NoteRequest { UserName = "test@gmail.com" });
            });
        }
    }
}