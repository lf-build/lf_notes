﻿using LendFoundry.Notes.Api.Controllers;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNet.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Notes.Api.Tests
{
    public class ApiControllerTests
    {
        private Mock<INoteRepository> NoteRepository { get; set; }
        private Mock<ILogger> logger { get; }
        private ApiController apiController { get; set; }
        private Mock<INoteService> mockNoteService { get; }

        public ApiControllerTests()
        {
            NoteRepository = new Mock<INoteRepository>();
            logger = new Mock<ILogger>();
            mockNoteService = new Mock<INoteService>();
        }

        [Fact]
        public async void GetAllNotes_Success()
        {
            IEnumerable<INote> serviceResponse = new List<INote>();
            mockNoteService.Setup(x => x.GetAllNotes(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(serviceResponse));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            var result = (HttpOkObjectResult)await apiController.AllNotes(It.IsAny<string>(), It.IsAny<string>());
            Assert.Equal(result.StatusCode, 200);
        }

        [Fact]
        public async void GetAllNotes_With_Null_EntityType_EntityId_Should_Thorw_ArgumentException()
        {
            mockNoteService.Setup(x => x.GetAllNotes(null, It.IsAny<string>())).Throws(new ArgumentException("entityType is mandatory"));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            var result = (ErrorResult)await apiController.AllNotes(null, It.IsAny<string>());
            Assert.Equal(result.StatusCode, 400);

            mockNoteService.Setup(x => x.GetAllNotes(It.IsAny<string>(), null)).Throws(new ArgumentException("entityId is mandatory"));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            result = (ErrorResult)await apiController.AllNotes(It.IsAny<string>(), null);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void GetNote_Success()
        {
            INote serviceResponse = new Note();
            mockNoteService.Setup(x => x.GetNote(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(serviceResponse));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            var result = (HttpOkObjectResult)await apiController.Note(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>());
            Assert.Equal(result.StatusCode, 200);
        }

        [Fact]
        public async void GetNote_With_Null_EntityType_EntityId_Should_Thorw_ArgumentException()
        {
            mockNoteService.Setup(x => x.GetNote(null, It.IsAny<string>(), It.IsAny<string>())).Throws(new ArgumentException("entityType is mandatory"));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            var result = (ErrorResult)await apiController.Note(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>());
            Assert.Equal(result.StatusCode, 400);

            mockNoteService.Setup(x => x.GetNote(It.IsAny<string>(), null, It.IsAny<string>())).Throws(new ArgumentException("entityId is mandatory"));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            result = (ErrorResult)await apiController.Note(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>());
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public void AddNote_Success()
        {
            var request = new NoteRequest()
            {
                Subject = "A Desc",
                Description = "B Desc"
            };

            var response = new Note()
            {
                Subject = "A Desc",
                Description = "B Desc",
                Id = Guid.NewGuid().ToString()
            };

            mockNoteService.Setup(x => x.AddNote(It.IsAny<string>(), It.IsAny<string>(), request)).Returns(response);
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            var result = (HttpOkObjectResult)apiController.AddNote(It.IsAny<string>(), It.IsAny<string>(), request);
            Assert.Equal(result.StatusCode, 200);
            Assert.NotNull(((Note)result.Value).Id);
        }

        [Fact]
        public void AddNote_EmptySubject_Thorw_ArgumentException()
        {
            var request = new NoteRequest()
            {
                Description = "B Desc"
            };

            mockNoteService.Setup(x => x.AddNote(It.IsAny<string>(), It.IsAny<string>(), request)).Throws(new ArgumentException("Subject is mandatory"));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            var result = (ErrorResult)apiController.AddNote(It.IsAny<string>(), It.IsAny<string>(), request);
            Assert.Equal(result.StatusCode, 400);
        }


        [Fact]
        public void AddNote_With_Null_EntityType_EntityId_Should_Thorw_ArgumentException()
        {
            var request = new NoteRequest()
            {
                Subject = "A Desc",
                Description = "B Desc"
            };

            var response = new Note()
            {
                Subject = "A Desc",
                Description = "B Desc",
                Id = Guid.NewGuid().ToString()
            };

            mockNoteService.Setup(x => x.AddNote(null, It.IsAny<string>(), request)).Throws(new ArgumentException("entityType is mandatory"));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            var result = (ErrorResult)apiController.AddNote(null, It.IsAny<string>(), request);
            Assert.Equal(result.StatusCode, 400);

            mockNoteService.Setup(x => x.AddNote(It.IsAny<string>(), null, request)).Throws(new ArgumentException("entityId is mandatory"));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            result = (ErrorResult)apiController.AddNote(It.IsAny<string>(), null, request);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public async void UpdateNote_Success()
        {
            var id = Guid.NewGuid().ToString();
            var request = new NoteRequest()
            {
                Subject = "A Desc",
                Description = "B Desc"
            };

            INote response = new Note()
            {
                Subject = "A Desc",
                Description = "B Desc",
                Id = id
            };

            mockNoteService.Setup(x => x.UpdateNote(It.IsAny<string>(), It.IsAny<string>(), id, request)).Returns(Task.FromResult(response));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            var result = (HttpOkObjectResult)await apiController.UpdateNote(It.IsAny<string>(), It.IsAny<string>(), id, request);
            Assert.Equal(result.StatusCode, 200);
            Assert.NotNull(((Note)result.Value).Id);
        }

        [Fact]
        public async void UpdateNote_EmptySubject_Thorw_ArgumentException()
        {
            var id = Guid.NewGuid().ToString();
            var request = new NoteRequest()
            {
                Description = "B Desc"
            };

            mockNoteService.Setup(x => x.UpdateNote(It.IsAny<string>(), It.IsAny<string>(), id, request)).Throws(new ArgumentException("Subject is mandatory"));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            var result = (ErrorResult)await apiController.UpdateNote(It.IsAny<string>(), It.IsAny<string>(), id, request);
            Assert.Equal(result.StatusCode, 400);
        }


        [Fact]
        public async void UpdateNote_With_Null_EntityType_EntityId_Should_Thorw_ArgumentException()
        {
            var id = Guid.NewGuid().ToString();
            var request = new NoteRequest()
            {
                Subject = "A Desc",
                Description = "B Desc"
            };

            INote response = new Note()
            {
                Subject = "A Desc",
                Description = "B Desc",
                Id = id
            };

            mockNoteService.Setup(x => x.UpdateNote(null, It.IsAny<string>(), id, request)).Throws(new ArgumentException("entityType is mandatory"));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            var result = (ErrorResult)await apiController.UpdateNote(null, It.IsAny<string>(), id, request);
            Assert.Equal(result.StatusCode, 400);

            mockNoteService.Setup(x => x.UpdateNote(It.IsAny<string>(), null, id, request)).Throws(new ArgumentException("entityId is mandatory"));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            result = (ErrorResult)await apiController.UpdateNote(It.IsAny<string>(), null, id, request);
            Assert.Equal(result.StatusCode, 400);
        }

        public async void DeleteNote_Success()
        {
            var id = Guid.NewGuid().ToString();
            mockNoteService.Setup(x => x.DeleteNote(It.IsAny<string>(), It.IsAny<string>(), id, It.IsAny<NoteRequest>())).Returns(Task.FromResult(true));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            var result = (HttpOkObjectResult)await apiController.DeleteNote(It.IsAny<string>(), It.IsAny<string>(), id, It.IsAny<NoteRequest>());
            Assert.Equal(result.StatusCode, 200);
            Assert.Equal(((bool)result.Value), true);
        }

        [Fact]
        public async void DeleteNote_With_Null_EntityType_EntityId_Should_Thorw_ArgumentException()
        {
            var id = Guid.NewGuid().ToString();

            mockNoteService.Setup(x => x.DeleteNote(null, It.IsAny<string>(), id, It.IsAny<NoteRequest>())).Throws(new ArgumentException("entityType is mandatory"));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            var result = (ErrorResult)await apiController.DeleteNote(null, It.IsAny<string>(), id, It.IsAny<NoteRequest>());
            Assert.Equal(result.StatusCode, 400);

            mockNoteService.Setup(x => x.DeleteNote(It.IsAny<string>(), null, id, It.IsAny<NoteRequest>())).Throws(new ArgumentException("entityId is mandatory"));
            apiController = new ApiController(mockNoteService.Object, logger.Object);
            result = (ErrorResult)await apiController.DeleteNote(It.IsAny<string>(), null, id, It.IsAny<NoteRequest>());
            Assert.Equal(result.StatusCode, 400);
        }
    }
}